<?php

namespace app\models;

use yii\db\ActiveRecord;

class Cars extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make', 'model', 'engine_volume'], 'required'],
            [['engine_volume'], 'number', 'numberPattern' => '/^\d[.,]\d$/', 'message'=>'The {attribute} field should have correct format, for example, 2.0'],
        ];
    }
}	